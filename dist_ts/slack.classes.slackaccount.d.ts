import * as plugins from './slack.plugins';
import { IMessageOptions } from './slack.classes.slackmessage';
export declare class SlackAccount {
    private postUrl;
    private updateUrl;
    private slackToken;
    constructor(slackTokenArg: string);
    sendMessage(optionsArg: {
        messageOptions: IMessageOptions;
        channelArg: string;
        ts?: string;
        mode: 'new' | 'threaded' | 'update';
    }): Promise<plugins.smartrequest.IExtendedIncomingMessage>;
}
