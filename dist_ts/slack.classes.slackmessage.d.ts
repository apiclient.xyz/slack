import * as plugins from './slack.plugins';
import { SlackAccount } from './slack.classes.slackaccount';
export interface IAttachmentField {
    title: string;
    value: string;
    short?: boolean;
}
export interface IMessageOptions {
    /**
     * "Required plain-text summary of the attachment."
     */
    fallback?: string;
    /**
     * a side color
     */
    color?: string;
    /**
     * a message to show above
     */
    pretext?: string;
    /**
     * author name of the attachment
     */
    author_name?: string;
    /**
     * a link to the author
     */
    author_link?: string;
    /**
     * a string to the author
     */
    author_icon?: string;
    /**
     * a title for the attachment
     */
    title?: string;
    /**
     * a link for the title
     */
    title_link?: string;
    /**
     * the main text of the message
     */
    text?: string;
    fields?: IAttachmentField[];
    image_url?: string;
    thumb_url?: string;
    footer?: string;
    footer_icon?: string;
    ts?: number;
}
export declare class SlackMessage {
    slackAccountRef: SlackAccount;
    messageOptions: IMessageOptions;
    channel: string;
    ts: string;
    requestRunning: plugins.smartpromise.Deferred<unknown>;
    constructor(slackAccountArg: SlackAccount, messageOptionsArg: IMessageOptions);
    updateAndSend(messageOptionsArg: IMessageOptions): Promise<void>;
    startThread(messageOptionsArg: IMessageOptions): Promise<void>;
    sendToRoom(channelNameArg: string, modeArg?: 'new' | 'update' | 'threaded'): Promise<void>;
}
